package com.example.consumer

import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service
import java.util.*

@Service
class RtoStatusUpdateConsumer(){

    companion object {
        private val log = LoggerFactory.getLogger(RtoStatusUpdateConsumer::class.java)
    }

    @KafkaListener(topics = ["headers_test"], containerFactory = "kafkaFactory")
    fun onMessage(@Payload data: String, @Header(value = KafkaHeaders.ACKNOWLEDGMENT) acknowledgment: Acknowledgment?, @Header(value = KafkaHeaders.RECEIVED_TIMESTAMP) timestamp: Long) {
        log.info("CONSUMER > Received RTO shipment status updates from shipment service. event: $data ${Date().time} $timestamp $acknowledgment")
        if(timestamp != null && timestamp < Date().time + 30 * 1000)
            acknowledgment?.nack(0, 10000)
        log.info("CONSUMER > not slept Received RTO shipment status updates from shipment service. event: $data ${Date().time}")
    }

}
