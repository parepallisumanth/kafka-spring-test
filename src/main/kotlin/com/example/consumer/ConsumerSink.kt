package com.example.consumer

import org.springframework.cloud.stream.annotation.Input
import org.springframework.messaging.SubscribableChannel

interface ConsumerSink {

    @Input
    fun headersQueueConsumer(): SubscribableChannel

    companion object {
        const val RTO_UPDATE_FROM_SHIPMENT = "headersQueueConsumer"
    }
}
