package com.example

import com.example.producer.ProducerGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.messaging.support.MessageBuilder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.util.*

@SpringBootApplication
class KafkaTestHeadersApplication {


}

@RestController
class TestCon {

	@Autowired
	lateinit var producerGateway: ProducerGateway

	@GetMapping
	fun run(@RequestParam s: String) {
//		val build = MessageBuilder.withPayload(s).setHeader("CreationTime", Date().time).build()
		producerGateway.send(s)
	}
}

fun main(args: Array<String>) {
	runApplication<KafkaTestHeadersApplication>(*args)
}
