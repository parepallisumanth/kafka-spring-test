package com.example.producer

import org.springframework.integration.annotation.Gateway
import org.springframework.integration.annotation.MessagingGateway

@MessagingGateway
interface ProducerGateway {

    @Gateway(requestChannel = ProducerSource.RTO_UPDATE_TO_TRIP)
    fun send(msg: String)

}
