package com.example.producer

import org.springframework.cloud.stream.annotation.Output
import org.springframework.messaging.SubscribableChannel

interface ProducerSource {

    @Output
    fun headersQueue(): SubscribableChannel

    companion object {
        const val RTO_UPDATE_TO_TRIP = "headersQueue"
    }
}
