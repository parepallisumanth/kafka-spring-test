package com.example

import com.example.consumer.ConsumerSink
import com.example.producer.ProducerSource
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.context.annotation.Configuration
import org.springframework.integration.annotation.IntegrationComponentScan

@Configuration
@EnableBinding(value = [ProducerSource::class, ConsumerSink::class])
@IntegrationComponentScan
class StreamConfig {
}
